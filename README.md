# E-commerce de vinos

Algunas observaciones:

- Al seleccionar una categoría se pueden marcar como activas mas de una
 de ellas, esto se debe a que los ids estan repetidos
 - Al filtrar por una subcategoría, el filtro sera aplicado solo al la ultima
 categoría en la que se escribió.
- No tuve mucho de trabajar en la versión responsive, los demas features fueron
completados y probados con pruebas automatizadas.

## Información del proyecto

 El proyecto fue realizado con el siguiente stack:
  - React
  - Redux
  - Bootstrap 4
  - Enzyme, para pruebas de integración
  - Jest, para pruebas unitarias

## Pasos para inicicar el proyecto:

#### Con npm en el directorio del proyecto
 - `npm install`, instalara todas las dependencias del proyecto
 - `npm start`, compilara archivos sass y correra el servidor local
 - `npm test`, correr las pruebas del proyecto
 - `npm test -- --coverage` para visualizar el % que abarcan las pruebas unitarias
 - Al iniciar el proyecto se abrirá una ventana en `localhost:3000`, allí se podra
 probar el proyecto

#### Con yarn en el directorio del proyecto
 - `yarn install`
 - `yarn start`
 - `yarn test`
 - `yarn test -- --coverage`
