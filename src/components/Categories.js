import React from 'react';
import { connect } from 'react-redux';
import MenuOptions from './MenuOptions';

export const Categories = (props) => (
    <MenuOptions sublevels={props.categories}/>
);

const mapStateToProps = (state) => ({
    categories: state.menuOptions,
});

export default connect(mapStateToProps)(Categories);
