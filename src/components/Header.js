import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import {connect} from 'react-redux';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem } from 'reactstrap';

export class Header extends Component {
    state = {
        isOpen: false
    };

    toggle =() => {
        this.setState((prevState) => ({
            isOpen: !prevState.isOpen
        }));
    };

    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="md" style={{marginBottom: 30}}>
                    <NavbarBrand style={{color: 'white'}} to="/">El Baratón</NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink to="/" exact className="nav-item nav-link" activeClassName="active">Productos</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/cart" className="nav-item nav-link" activeClassName="active">
                                    <i className="fa fa-cart-plus fa-lg text-secondary c-pointer" aria-hidden="true"/>
                                    <span className="badge badge-danger nav-bar__badge">{this.props.cartCount}</span>
                                </NavLink>

                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    cartCount: state.cart.length
});

export default connect(mapStateToProps)(Header);
