import React, { Component } from 'react';
import { connect } from 'react-redux';
import MenuOption from './MenuOption';
import { Input } from 'reactstrap';
import { setSubLevel, setSubLevelsQuery } from '../actions/filters.action';

export class MenuOptions extends Component {
    state = {
        textSearch: '',
    };

    onTextFilterChange = (e) => {
        const value = e.target.value;

        this.setState((prevState) => {
            if (!value) {
                this.props.setSubLevelsQuery();
            } else {
                this.props.setSubLevelsQuery({ subLevels: this.props.sublevels, textSearch: value });
            }
            this.props.setSubLevel();
            return { textSearch: value }
        });
    };

    render() {
        return (
            <div>
                <ul className="menu-options-list">
                    {
                        this.props.sublevels &&
                        this.props.sublevels.map(sublevel => <MenuOption key={sublevel.id} sublevel={sublevel}/>)
                    }
                </ul>
                <Input placeholder="Type to filter" value={this.state.textSearch} onChange={this.onTextFilterChange}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    subLevelsQuery: state.filters.subLevelsQuery,
});

const mapDispatchToProps = (dispatch) => ({
    setSubLevelsQuery: (subLevelsQuery) => {
        dispatch(setSubLevelsQuery(subLevelsQuery));
    },
    setSubLevel: (subLevel) => {
        dispatch(setSubLevel(subLevel));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuOptions);
