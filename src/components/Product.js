import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Input,
} from 'reactstrap';
import numeral from 'numeral';
import { editProduct, removeProduct } from '../actions/cart.action';

export class Product extends Component {
    onQuantityChange = (e) => {
        const value = e.target.value;
        if (!value || value.match(/^\d+$/)) {
            this.props.editProduct(this.props.product.id, { quantity: !value || parseInt(value) === 0 ? 1 : value });
        }
    };

    removeProduct = () => {
        this.props.removeProduct(this.props.product.id);
    };

    calculateTotal = () => {
        const price = numeral(this.props.product.price).value();

        return numeral(price * this.props.product.quantity).format('$0,0.00');
    };

    render() {
        const { name, price, quantity } = this.props.product;
        return (
            <tr>
                <td className="text-center">
                    <i className="fa fa-trash fa-2x text-danger c-pointer"
                       aria-hidden="true" onClick={this.removeProduct}/>
                </td>
                <td className="text-center text-capitalize">{name}</td>
                <td className="text-center">{price}</td>
                <td style={{width: '20%'}}>
                    <Input className="text-center"
                           type="number"
                           placeholder="Cantidad"
                           value={quantity}
                           onChange={this.onQuantityChange}/>
                </td>
                <td className="text-center">
                    {this.calculateTotal()}
                </td>
            </tr>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    editProduct: (id, product) => {
        dispatch(editProduct(id, product));
    },
    removeProduct: (id) => {
        dispatch(removeProduct(id));
    },
});

export default connect(undefined, mapDispatchToProps)(Product);
