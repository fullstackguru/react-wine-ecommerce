import React from 'react';
import { connect } from 'react-redux';
import { Container, Row, Col, Table } from 'reactstrap';

import ProductListItem from './ProductListItem';
import selectProducts from '../selectors/products.filter';
import ProductListFilters from './ProductListFilters';
import Categories from './Categories';

export const ProductList = (props) => (
    <Container>
        <Row>
            <Col xs={5} sm={4} lg={2}>
                <Categories />
                <ProductListFilters/>
            </Col>
            <Col xs={7} sm={8} lg={10}>
                <Table striped bordered>
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th />
                    </tr>
                    </thead>
                    <tbody>
                    {
                        props.products.map(product => <ProductListItem key={product.id} product={product}/>)
                    }
                    </tbody>
                </Table>
            </Col>
        </Row>
    </Container>

);

const mapStateToProps = (state) => {
    const products = selectProducts(state.products, state.filters);
    return {
        products: products,
        productsCount: products.length,
    };
};

export default connect(mapStateToProps)(ProductList);
