import React, { Component } from 'react';
import { connect } from 'react-redux';
import { isProductOnCart } from '../selectors/products.filter';
import { addProduct, removeProduct } from '../actions/cart.action';

export class ProductListItem extends Component {
    addToCart = () => {
        const product = {...this.props.product, quantity: 1};
        this.props.addProduct(product);
    };

    removeFromCart = () => {
        this.props.removeProduct(this.props.product.id);
    };

    render() {
        const {name, available, quantity, price} = this.props.product;
        return (
            <tr>
                <td className="text-capitalize">
                    {name}
                    <p className="table__note">{available ? 'Disponible' : 'No disponible'}</p>
                </td>
                <td>{quantity}</td>
                <td>{price}</td>
                <td>
                    {!this.props.isProductOnCart(this.props.product)
                        ? <i className="fa fa-cart-plus fa-2x text-secondary c-pointer"
                             aria-hidden="true" onClick={this.addToCart}/>
                        : <i className="fa fa-cart-plus fa-2x text-success c-pointer"
                             aria-hidden="true" onClick={this.removeFromCart}/>
                    }
                </td>
            </tr>
        );
    }
}

const mapStateToProps = (state) => ({
    isProductOnCart: (product) => {
        return isProductOnCart(state.cart, product);
    },
});

const mapDispatchToProps = (dispatch) => ({
    addProduct: (product) => {
        dispatch(addProduct(product));
    },
    removeProduct: (id) => {
        dispatch(removeProduct(id));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductListItem);
