import React from 'react';
import ProductList from './ProductList';

export const ProductPage = () => (
  <div>
      <ProductList />
  </div>
);

export default ProductPage;
