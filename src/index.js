import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './styles/styles.css';

import AppRouter from './routes/AppRouter';
import configureStore from './store/configureStore';
import products from './data/products.json';
import menuOptions from './data/categories.json';
import { setProducts } from './actions/products.action';

import { saveProducts, loadProducts } from './utils/storage';
import { addProducts } from './actions/cart.action';
import { setMenuOptions } from './actions/menu-options.action';

const store = configureStore();

const p = loadProducts();

store.dispatch(setProducts(products));
store.dispatch(setMenuOptions(menuOptions));
store.dispatch(addProducts(p));

store.subscribe(() => {
    const cart = store.getState().cart;
    saveProducts(cart);
});

const jsx = (
    <Provider store={store}>
        <AppRouter/>
    </Provider>
);

ReactDOM.render(jsx, document.getElementById('root'));
