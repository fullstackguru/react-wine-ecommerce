const cartReducerDefaultState = [];

const cartReducer = (state = cartReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_PRODUCTS':
            return state.concat(action.products);
        case 'ADD_PRODUCT':
            return state.concat(action.product);
        case 'EDIT_PRODUCT':
            return state.map(product => {
               if (product.id === action.id) {
                   return {...product, ...action.updates}
               }
               return product;
            });
        case 'REMOVE_PRODUCTS':
            return [];
        case 'REMOVE_PRODUCT':
            return state.filter(product => product.id !== action.id);
        default:
            return state;
    }
};

export default cartReducer;
