const menuOptionsDefaultState = [];

const menuOptionsReducer = (state = menuOptionsDefaultState, action) => {
    switch (action.type) {
        case 'SET_MENU_OPTIONS':
            return state.concat(action.menuOptions);
        default:
            return state;
    }
};

export default menuOptionsReducer;
