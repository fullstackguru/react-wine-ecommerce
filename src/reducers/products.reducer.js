const productsReducerDefaultState = [];

const productsReducer = (state = productsReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_PRODUCTS':
            return state.concat(action.products);
        default:
            return state;
    }
};

export default productsReducer;
