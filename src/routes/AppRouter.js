import React from 'react';
import { Router, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';

import ProductPage from '../components/ProductsPage';
import CartPage from '../components/CartPage';
import PublicRoute from './PublicRoute';

const history = createHistory();

const AppRouter = () => (
    <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" exact={true} component={ProductPage}/>
                <PublicRoute path="/cart" component={CartPage}/>
            </Switch>
        </div>
    </Router>
);

export default AppRouter;
