import numeral from 'numeral';

const getVisibleProducts = (products, { subLevel = {}, subLevelsQuery = {}, availableStatus = true, startPrice = '', endPrice = '', quantity = '', sortBy }) => {
    return products.filter(product => {
        startPrice = typeof startPrice === 'undefined' ? undefined : numeral(startPrice).value();
        endPrice = typeof endPrice === 'undefined' ? undefined : numeral(endPrice).value();

        const productPrice = numeral(product.price).value();

        const subLevelIdMatch = !subLevel.id ? true : subLevel.id === product.sublevel_id;
        const subLevelsQueryMatch = !subLevelsQuery.textSearch ? true : matchSubLevelAndProduct(subLevelsQuery.subLevels, subLevelsQuery.textSearch, product);
        const availableMatch = product.available === availableStatus;
        const startPriceMatch = !startPrice ? true : productPrice >= parseFloat(startPrice);
        const endPriceMatch = !endPrice ? true : productPrice <= parseFloat(endPrice);
        const quantityMatch = !quantity ? true : product.quantity === parseInt(quantity, 10);

        return subLevelIdMatch && availableMatch && startPriceMatch && endPriceMatch && quantityMatch && subLevelsQueryMatch;
    }).sort((pA, pB) => {
        // Use undefined in sortBy to be easy to test
        if (typeof sortBy === 'undefined') return 0;

        if (sortBy === 'available') {
            return pA.available ? -1 : 1;
        }

        if (sortBy === 'quantity') {
            return pA.quantity < pB.quantity ? 1 : -1;
        }

        return numeral(pA.price).value() > numeral(pB.price).value() ? 1 : -1;
    });
};

export const matchSubLevelAndProduct = (subLevels, textToSearch, product) => {
    if (! subLevels.length) return false;

    const productName = product.name.toLowerCase();

    // Get all subLevel ids
    const ids = subLevels.reduce((current, subLevel) => current.concat(subLevel.id) , []);

    // Check if a subLevel id match and product name
    return ~ids.indexOf(product.sublevel_id) && productName.includes(textToSearch.toLowerCase());
};

export const isProductOnCart = (products, product) => !! products.find(p => p.id === product.id);

export default getVisibleProducts;
