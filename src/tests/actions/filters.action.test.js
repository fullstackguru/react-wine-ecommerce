import {
    setAvailableStatus,
    setEndPrice,
    setQuantity,
    setStartPrice,
    setSubLevel, setSubLevelsQuery,
    sortByAvailableStatus,
    sortByPrice,
    sortByStock,
} from '../../actions/filters.action';

test('should generate set subLevels query action object', () => {
   const subLevel = {id: 2, name: 'Con azúcar'};
   const subLevelsQuery = {subLevels: [subLevel], textSearch: 'Hello World'};
   const action = setSubLevelsQuery(subLevelsQuery);
   expect(action).toEqual({
       type: 'SET_SUB_LEVELS_QUERY',
       subLevelsQuery
   })
});

test('should generate set level id action object', () => {
    const subLevel = { id: 2, name: 'Con azúcar' };
    const action = setSubLevel(subLevel);
    expect(action).toEqual({
        type: 'SET_SUB_LEVEL',
        subLevel,
    });
});

test('should generate set available status action object', () => {
    const availableStatus = true;
    const action = setAvailableStatus(availableStatus);
    expect(action).toEqual({ type: 'SET_AVAILABLE_STATUS', availableStatus });
});

test('should generate set start price action object', () => {
    const startPrice = '$17,001';
    const action = setStartPrice(startPrice);
    expect(action).toEqual({ type: 'SET_START_PRICE', startPrice });
});

test('should generate set end price action object', () => {
    const endPrice = '$17,001';
    const action = setEndPrice(endPrice);
    expect(action).toEqual({ type: 'SET_END_PRICE', endPrice });
});

test('should generate set quantity action object', () => {
    const quantity = 698;
    const action = setQuantity(quantity);
    expect(action).toEqual({type: 'SET_QUANTITY', quantity})
});

test('should generate sort by price action object', () => {
   const action = sortByPrice();
   expect(action).toEqual({type: 'SORT_BY_PRICE'});
});

test('should generate sort by available status action object', () => {
    const action = sortByAvailableStatus();
    expect(action).toEqual({type: 'SORT_BY_AVAILABLE_STATUS'});
});

test('should generate sort by stock action object', () => {
    const action = sortByStock();
    expect(action).toEqual({type: 'SORT_BY_STOCK'});
});
