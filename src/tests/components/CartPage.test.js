import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { CartPage } from '../../components/CartPage';
import { products } from '../fixtures/products';
import { calculateTotal } from '../../utils/product';

let total = calculateTotal(products);
let removeProducts;

beforeEach(() => {
    removeProducts = jest.fn();
});

test('should render product page correctly without products', () => {
    const wrapper = shallow(<CartPage products={[]}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should render product page correctly with products', () => {
    const wrapper = shallow(<CartPage products={products}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should handle toggle modal', () => {
    const wrapper = shallow(<CartPage products={products} total={total} removeProducts={removeProducts}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
    wrapper.find('Button').simulate('click');
    expect(wrapper.state('modal')).toBe(true);
    expect(removeProducts).toHaveBeenCalled();
});
