import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import {Categories} from '../../components/Categories';
import {menuOptions} from '../fixtures/menu-options';

test('should render categories correctly', () => {
   const wrapper = shallow(<Categories categories={menuOptions}/>);
    expect(toJSON(wrapper)).toMatchSnapshot();
});
