import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { MenuOptions } from '../../components/MenuOptions';

let wrapper, setSubLevelsQuery, setSubLevel;
const subLevels = [
    {
        'id': 1,
        'name': 'Gaseosas',
        'sublevels': [
            {
                'id': 2,
                'name': 'Con azúcar',
            },
            {
                'id': 3,
                'name': 'Sin azúcar',
            },
        ],
    },
];

beforeEach(() => {
    setSubLevelsQuery = jest.fn();
    setSubLevel = jest.fn();
    wrapper = shallow(<MenuOptions sublevels={subLevels}
                                   subLevelsQuery={{}}
                                   setSubLevelsQuery={setSubLevelsQuery}
                                   setSubLevel={setSubLevel}/>);
});

test('should render MenuOptions correctly with sublevels', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should render MenuOptions correctly without sublevels', () => {
    wrapper = shallow(<MenuOptions sublevels={undefined}
                                   subLevelsQuery={{}}
                                   setSubLevelsQuery={setSubLevelsQuery}
                                   setSubLevel={setSubLevel}/>);
});

test('should handle textSearch change', () => {
    const value = 'Text to search';
    wrapper.find('Input').simulate('change', {
        target: { value },
    });

    expect(setSubLevel).toHaveBeenCalled();
    expect(setSubLevelsQuery).toHaveBeenLastCalledWith({ subLevels, textSearch: value });
    expect(wrapper.state('textSearch')).toBe(value);
});

test('should handle remove subLevels when textSearch is empty', () => {
    let value = 'Text to search';
    wrapper.find('Input').simulate('change', {
        target: { value },
    });

    value = '';
    wrapper.find('Input').simulate('change', {
        target: { value },
    });
    expect(setSubLevel).toHaveBeenCalled();
    expect(setSubLevelsQuery).toHaveBeenLastCalledWith();
    expect(wrapper.state('textSearch')).toBe(value);
});
