import React from 'react';
import { shallow } from 'enzyme';
import toJSON from 'enzyme-to-json';
import { ProductListItem } from '../../components/ProductListItem';
import { products } from '../fixtures/products';

let wrapper, product, isProductOnCart, addProduct, removeProduct;

beforeEach(() => {
    product = products[0];
    addProduct = jest.fn();
    removeProduct = jest.fn();
    isProductOnCart = false;
    wrapper = shallow(<ProductListItem product={product}
                                       addProduct={addProduct}
                                       removeProduct={removeProduct}
                                       isProductOnCart={() => false}/>);
});

test('should render ProductListItem correctly when product is not in the cart', () => {
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should render ProductListItem correctly when product is in the cart', () => {
    wrapper.setProps({ isProductOnCart: () => true });
    expect(toJSON(wrapper)).toMatchSnapshot();
});

test('should handle add product to the cart', () => {
    wrapper.find('i').simulate('click');
    expect(addProduct).toHaveBeenLastCalledWith({ ...product, quantity: 1 });
});

test('should handle remove product to the cart', () => {
    wrapper.setProps({ isProductOnCart: () => true });
    wrapper.find('i').simulate('click');
    expect(removeProduct).toHaveBeenLastCalledWith(product.id);
});
