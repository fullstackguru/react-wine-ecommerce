const products = [
    {
        'quantity': 308,
        'price': '$8,958',
        'available': false,
        'sublevel_id': 3,
        'name': 'aute',
        'id': '58b5a5b1b6b6c7aacc25b3fb',
    },
    {
        'quantity': 698,
        'price': '$17,001',
        'available': true,
        'sublevel_id': 10,
        'name': 'eiusmod',
        'id': '58b5a5b18607b1071fb5ab5b',
    },
    {
        'quantity': 449,
        'price': '$6,864',
        'available': true,
        'sublevel_id': 7,
        'name': 'proident',
        'id': '58b5a5b13881e97291384813',
    },
    {
        'quantity': 891,
        'price': '$5,450',
        'available': true,
        'sublevel_id': 3,
        'name': 'mollit',
        'id': '58b5a5b117bf36cf8aed54ab',
    },
    {
        "quantity": 7588888,
        "price": "$16,235",
        "available": true,
        "sublevel_id": 3,
        "name": "id",
        "id": "58b5a5b1101a59975b4c30b2"
    }
];

export {products};
