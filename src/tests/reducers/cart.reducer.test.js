import cartReducer from '../../reducers/cart.reducer';
import {products} from '../fixtures/products';

let product;

beforeEach(() => {
    product = products[0];
});

test('should add products to the cart', () => {
    const state = cartReducer([], {type: 'ADD_PRODUCTS', products});
    expect(state).toEqual(products);
});

test('should add new product to the cart', () => {
    const state = cartReducer([], {type: 'ADD_PRODUCT', product});
    expect(state).toEqual([product]);
});

test('should edit product from the cart', () => {
    const updates = {...product, name: 'New name'};
    const state = cartReducer([product], {type:'EDIT_PRODUCT', id: product.id, updates});
    expect(state).toEqual([updates]);
});

test('should remove products from the cart', () => {
   const state = cartReducer(products, {type: 'REMOVE_PRODUCTS'});
   expect(state).toEqual([]);
});

test('should remove product from the cart', () => {
    const id = product.id;
    const state = cartReducer([product], {type: 'REMOVE_PRODUCT', id});
    expect(state).toEqual([]);
});
