import menuOptionsReducer from '../../reducers/menu-options.reducer';
import {menuOptions} from '../fixtures/menu-options';

test('should set up products into the state', () => {
    const state = menuOptionsReducer([], {type: 'SET_MENU_OPTIONS', menuOptions});
    expect(state).toEqual(menuOptions);
});
