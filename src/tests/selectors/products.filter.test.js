import selectProducts, { isProductOnCart, matchSubLevelAndProduct } from '../../selectors/products.filter';
import { products } from '../fixtures/products';

let subLevel, subLevels;

beforeEach(() => {
    subLevel = { id: 3, name: 'Sin azúcar' };
    subLevels = [
        {
            'id': 2,
            'name': 'Con azúcar',
        },
        {
            'id': 3,
            'name': 'Sin azúcar',
        },
    ];
});

test('should filter by subLevel id', () => {
    const filters = {
        subLevel,
    };

    const result = selectProducts(products, filters);
    expect(result).toEqual([products[3], products[4]]);
});

test('should filter by available status', () => {
    const filters = {
        availableStatus: true,
    };

    const result = selectProducts(products, filters);
    expect(result.length).toBe(4);
});

test('should filter by start price', () => {
    const filters = {
        startPrice: '$8,958',
    };

    const result = selectProducts(products, filters);
    expect(result).toEqual([products[1], products[4]]);
});

test('should filter by end price', () => {
    const filters = {
        endPrice: '$8,000',
    };

    const result = selectProducts(products, filters);
    expect(result).toEqual([products[2], products[3]]);
});

test('should filter by quantity', () => {
    const filters = {
        quantity: 698,
    };

    const result = selectProducts(products, filters);
    expect(result).toEqual([products[1]]);
});

test('should sort by price', () => {
    const filter = {
        sortBy: 'price',
    };

    const result = selectProducts(products, filter);
    expect(result).toEqual([products[3], products[2], products[4], products[1]]);
});

test('should sort by available status', () => {
    const filter = {
        sortBy: 'available',
    };

    const result = selectProducts(products, filter);
    expect(result).toEqual([products[1], products[2], products[3], products[4]]);
});

test('should sort by quantity', () => {
    const filter = {
        sortBy: 'quantity',
    };

    const result = selectProducts(products, filter);
    expect(result).toEqual([products[4], products[3], products[1], products[2]]);
});

test('should check if product is in the cart', () => {
    const product = products[0];

    const result = isProductOnCart(products, product);
    expect(result).toBe(true);
});

test('should check if the product is not in the cart', () => {
    const product = { id: '123' };

    const result = isProductOnCart(products, product);
    expect(result).toBe(false);
});

test('should filter by sublevels ', () => {
   const filters = {
       subLevelsQuery: {
           subLevels,
           textSearch: 'mollit'
       }
   };

   const result = selectProducts(products, filters);
   expect(result).toEqual([products[3]]);
});

test('should matchSubLevelAndProduct filter by sublevel and product name', () => {
    const product = { name: 'Text', sublevel_id: 2 };
    const result = matchSubLevelAndProduct(subLevels, 'text', product);
    expect(result).toBe(true);

    const product2 = { name: 'Text', sublevel_id: 4 };
    const result2 = !!matchSubLevelAndProduct(subLevels, 'text', product2);
    expect(result2).toBe(false);
});
