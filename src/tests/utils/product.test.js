import numeral from 'numeral';
import {products} from '../fixtures/products';
import { calculateTotal, parsePrice } from '../../utils/product';

test('should calculate total of products base on price and quantity', () => {
    const p1 = products[0], p2 = products[1];
    const total = (parsePrice(p1) * p1.quantity) + (parsePrice(p2) * p2.quantity);
    const p = [products[0], products[1]];
    const result = calculateTotal(p);
    expect(result).toBe(total);
});

test('should parse dolar price format to number', () => {
    const result = parsePrice(products[0]);
    expect(result).toBe(8958);
});
