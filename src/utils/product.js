import numeral from 'numeral';

export const calculateTotal = (products) => products.reduce((current, product) => current + (parsePrice(product) * product.quantity), 0);

export const parsePrice = (product) => numeral(product.price).value();
